= Dive Into Python 3
:doctype:   book
:docinfo:
:toc:
:toclevels: 2
:pagenums:
:source-highlighter: coderay
:front-cover-image: image:book/cover2.jpg[width=1050,height=1600]

include::book/preface.asc[]

include::book/01-whats-new/whats-new.asc[]

include::book/02-installing-python/installing-python.asc[]
include::book/03-your-first-python-program/your-first-python-program.asc[]
include::book/04-native-datatypes/native-datatypes.asc[]
include::book/05-comprehensions/comprehensions.asc[]
include::book/06-strings/strings.asc[]
include::book/07-regular-expressions/regular-expressions.asc[]
include::book/08-generators/generators.asc[]
// 09 include::book/09-iterators/iterators.asc[]
// 10 include::book/advanced-iterators.asc[]
// 11 include::book/unit-testing.asc[]
// 12 include::book/refactoring.asc[]
// 13 include::book/files.asc[]
// 14 include::book/xml.asc[]
// 15 include::book/serializing.asc[]
// 16 include::book/http-web-services.asc[]
// 17 include::book/case-study-porting-chardet-to-python-3.asc[]
// 18 include::book/packaging.asc[]
// 19 include::book/porting-code-to-python-3-with-2to3.asc[]
// 20 include::book/special-method-names.asc[]
// 21 include::book/where-to-go-from-here.asc[]
// 22 include::book/troubleshooting.asc[]
