[[generators]]
=== generators

Wouldn’t it be grand to have a generic `plural()` function that parses the rules file? Get rules, check for a match, apply appropriate transformation, go to next rule. That’s all the `plural()` function has to do, and that’s all the `plural()` function should do.

[link:examples/plural5.py[download `plural5.py`]]

[source,nd,pp]
----
def rules(rules_filename):
    with open(rules_filename, encoding='utf-8') as pattern_file:
        for line in pattern_file:
            pattern, search, replace = line.split(None, 3)
            yield build_match_and_apply_functions(pattern, search, replace)

def plural(noun, rules_filename='plural5-rules.txt'):
    for matches_rule, apply_rule in rules(rules_filename):
        if matches_rule(noun):
            return apply_rule(noun)
    raise ValueError('no matching rule for {0}'.format(noun))
----

How the heck does _that_ work? Let’s look at an interactive example first.

[source,screen]
----
>>> def make_counter(x):
...     print('entering make_counter')
...     while True:
...         yield x                    # <1>
...         print('incrementing x')
...         x = x + 1
...
>>> counter = make_counter(2)          # <2>
>>> counter                            # <3>
<generator object at 0x001C9C10>
>>> next(counter)                      # <4>
entering make_counter
2
>>> next(counter)                      # <5>
incrementing x
3
>>> next(counter)                      # <6>
incrementing x
4
----

<1> The presence of the `yield` keyword in `make_counter` means that this is not a normal function. It is a special kind of function which generates values one at a time. You can think of it as a resumable function. Calling it will return a _generator_ that can be used to generate successive values of x.
<2> To create an instance of the `make_counter` generator, just call it like any other function. Note that this does not actually execute the function code. You can tell this because the first line of the `make_counter()` function calls `print()`, but nothing has been printed yet.
<3> The `make_counter()` function returns a generator object.
<4> The `next()` function takes a generator object and returns its next value. The first time you call `next()` with the counter generator, it executes the code in `make_counter()` up to the first `yield` statement, then returns the value that was yielded. In this case, that will be `2`, because you originally created the generator by calling `make_counter(2)`.
<5> Repeatedly calling `next()` with the same generator object resumes exactly where it left off and continues until it hits the next `yield` statement. All variables, local state, __&__c. are saved on `yield` and restored on `next()`. The next line of code waiting to be executed calls `print()`, which prints incrementing x. After that, the statement `x = x + 1`. Then it loops through the `while` loop again, and the first thing it hits is the statement `yield x`, which saves the state of everything and returns the current value of x (now `3`).
<6> The second time you call `next(counter)`, you do all the same things again, but this time x is now `4`.

Since `make_counter` sets up an infinite loop, you could theoretically do this forever, and it would just keep incrementing x and spitting out values. But let’s look at more productive uses of generators instead.
