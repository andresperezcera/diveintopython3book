[[a-fibonacci-generator]]
==== A fibonacci generator

“yield” pauses a function. “next()” resumes where it left off.

[link:examples/fibonacci.py[download `fibonacci.py`]]

[source,pp]
----
def fib(max):
    a, b = 0, 1          # <1>
    while a < max:
        yield a          # <2>
        a, b = b, a + b  # <3>
----

<1> The Fibonacci sequence is a sequence of numbers where each number is the sum of the two numbers before it. It starts with 0 and `1`, goes up slowly at first, then more and more rapidly. To start the sequence, you need two variables: a starts at 0, and b starts at `1`.
<2> a is the current number in the sequence, so yield it.
<3> b is the next number in the sequence, so assign that to a, but also calculate the next value (`a + b`) and assign that to b for later use. Note that this happens in parallel; if a is `3` and b is `5`, then `a, b = b, a + b` will set a to `5` (the previous value of b) and b to `8` (the sum of the previous values of a and b).

So you have a function that spits out successive Fibonacci numbers. Sure, you could do that with recursion, but this way is easier to read. Also, it works well with `for` loops.

[source,screen]
----
>>> from fibonacci import fib
>>> for n in fib(1000):      # <1>
...     print(n, end=' ')    # <2>
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987
>>> list(fib(1000))          # <3>
[0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]
----

<1> You can use a generator like `fib()` in a `for` loop directly. The `for` loop will automatically call the `next()` function to get values from the `fib()` generator and assign them to the `for` loop index variable (n).
<2> Each time through the `for` loop, n gets a new value from the `yield` statement in `fib()`, and all you have to do is print it out. Once `fib()` runs out of numbers (a becomes bigger than max, which in this case is `1000`), then the `for` loop exits gracefully.
<3> This is a useful idiom: pass a generator to the `list()` function, and it will iterate through the entire generator (just like the `for` loop in the previous example) and return a list of all the values.
