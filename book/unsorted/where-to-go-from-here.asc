Where to Go From Here - Dive Into Python 3

  

You are here: link:index.html[Home] ‣
link:table-of-contents.html#where-to-go-from-here[Dive Into Python 3] ‣

[[where-to-go-from-here]]
Where To Go From Here
---------------------

____________________________________________________________________
❝ Go forth on your path, as it exists only through your walking. ❞ +
— St. Augustine of Hippo (attributed)
____________________________________________________________________

 

[[things-to-read]]
Things to Read
~~~~~~~~~~~~~~

Unfortunately, I can not write cover every facet of Python 3 in this
book. Fortunately, there are many wonderful, freely available tutorials
available elsewhere.

Decorators:

* http://programmingbits.pythonblogs.com/27_programmingbits/archive/50_function_decorators.html[Function
Decorators] by Ariel Ortiz
* http://programmingbits.pythonblogs.com/27_programmingbits/archive/51_more_on_function_decorators.html[More
on Function Decorators] by Ariel Ortiz
* http://www.ibm.com/developerworks/linux/library/l-cpdecor.html[Charming
Python: Decorators make magic easy] by David Mertz
* http://docs.python.org/reference/compound_stmts.html#function[Function
Definitions] in the official Python documentation

Properties:

* http://adam.gomaa.us/blog/2008/aug/11/the-python-property-builtin/[The
Python `property` builtin] by Adam Gomaa
* http://tomayko.com/writings/getters-setters-fuxors[Getters/Setters/Fuxors]
by Ryan Tomayko
* http://docs.python.org/library/functions.html#property[`property()`
function] in the official Python documentation

Descriptors:

* http://users.rcn.com/python/download/Descriptor.htm[How-To Guide For
Descriptors] by Raymond Hettinger
* http://www.ibm.com/developerworks/linux/library/l-python-elegance-2.html[Charming
Python: Python elegance and warts, Part 2] by David Mertz
* http://www.informit.com/articles/printerfriendly.aspx?p=1309289[Python
Descriptors] by Mark Summerfield
* http://docs.python.org/3.1/reference/datamodel.html#invoking-descriptors[Invoking
Descriptors] in the official Python documentation

Threading _&_ multiprocessing:

* http://docs.python.org/3.1/library/threading.html[`threading` module]
* http://www.doughellmann.com/PyMOTW/threading/[`threading` — Manage
concurrent threads]
* http://docs.python.org/3.1/library/multiprocessing.html[`multiprocessing`
module]
* http://www.doughellmann.com/PyMOTW/multiprocessing/[`multiprocessing` — Manage
processes like threads]
* http://jessenoller.com/2009/02/01/python-threads-and-the-global-interpreter-lock/[Python
threads and the Global Interpreter Lock] by Jesse Noller
* http://blip.tv/file/2232410[Inside the Python GIL (video)] by David
Beazley

Metaclasses:

* http://www.ibm.com/developerworks/linux/library/l-pymeta.html[Metaclass
programming in Python] by David Mertz and Michele Simionato
* http://www.ibm.com/developerworks/linux/library/l-pymeta2/[Metaclass
programming in Python, Part 2] by David Mertz and Michele Simionato
* http://www.ibm.com/developerworks/linux/library/l-pymeta3.html[Metaclass
programming in Python, Part 3] by David Mertz and Michele Simionato

In addition, Doug Hellman’s
http://www.doughellmann.com/PyMOTW/contents.html[Python Module of the
Week] is a fantastic guide to many of the modules in the Python standard
library.

[[code]]
Where To Look For Python 3-Compatible Code
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As Python 3 is relatively new, there is a dearth of compatible
libraries. Here are some of the places to look for code that works with
Python 3.

* http://pypi.python.org/pypi?:action=browse&c=533&show=all[Python
Package Index: list of Python 3 packages]
* http://code.activestate.com/recipes/langs/python/tags/meta:min_python_3/[Python
Cookbook: list of Python 3 recipes]
* http://code.google.com/hosting/search?q=label:python3[Google Project
Hosting: list of projects tagged “python3”]
* http://sourceforge.net/search/?words=%22python+3%22[SourceForge: list
of projects matching “Python 3”]
* http://github.com/search?type=Repositories&language=python&q=python3[GitHub:
list of projects matching “python3”] (also,
http://github.com/search?type=Repositories&language=python&q=python+3[list
of projects matching “python 3”])
* http://bitbucket.org/repo/all/?name=python3[BitBucket: list of
projects matching “python3”] (and
http://bitbucket.org/repo/all/?name=python+3[those matching “python 3”])

link:special-method-names.html[☜] link:troubleshooting.html[☞]

© 2001–11 link:about.html[Mark Pilgrim]
