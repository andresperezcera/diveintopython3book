[[booleans]]
=== Booleans

You can use virtually any expression in a boolean context.

Booleans are either true or false. Python has two constants, cleverly named `True` and `False`, which can be used to assign boolean values directly. Expressions can also evaluate to a boolean value. In certain places (like `if` statements), Python expects an expression to evaluate to a boolean value. These places are called __boolean contexts__. You can use virtually any expression in a boolean context, and Python will try to determine its truth value. Different datatypes have different rules about which values are true or false in a boolean context. (This will make more sense once you see some concrete examples later in this chapter.)

For example, take this snippet from <<divingin-your-first-python-program,`humansize.py`>>:

[source,nd,pp]
----
if size < 0:
    raise ValueError('number must be non-negative')
----

size is an integer, 0 is an integer, and `<` is a numerical operator. The result of the expression `size < 0` is always a boolean. You can test this yourself in the Python interactive shell:

[source,console]
----
>>> size = 1
>>> size < 0
False
>>> size = 0
>>> size < 0
False
>>> size = -1
>>> size < 0
True
----

Due to some legacy issues left over from Python 2, booleans can be treated as numbers. `True` is `1`; `False` is 0.

[source,console]
----
>>> True + True
2
>>> True - False
1
>>> True * False
0
>>> True / False
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: int division or modulo by zero
----

Ew, ew, ew! Don’t do that. Forget I even mentioned it.
