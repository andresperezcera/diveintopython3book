[[windows]]
=== Installing on Microsoft Windows


Windows comes in two architectures these days: 32-bit and 64-bit. Of
course, there are lots of different _versions_ of Windows — XP, Vista,
Windows 7 — but Python runs on all of them. The more important
distinction is 32-bit v. 64-bit. If you have no idea what architecture
you’re running, it’s probably 32-bit.

Visit http://python.org/download/[`python.org/download/`] and download
the appropriate Python 3 Windows installer for your architecture. Your
choices will look something like this:

* *Python 3.1 Windows installer* (Windows binary — does not include
source)
* *Python 3.1 Windows AMD64 installer* (Windows AMD64 binary — does not
include source)

I don’t want to include direct download links here, because minor
updates of Python happen all the time and I don’t want to be responsible
for you missing important updates. You should always install the most
recent version of Python 3.x unless you have some esoteric reason not
to.


. Once your download is complete, double-click the `.msi` file. Windows will pop up a security alert, since you’re about to be running executable code. The official Python installer is digitally signed by the http://www.python.org/psf/[Python Software Foundation], the non-profit corporation that oversees Python development. Don’t accept imitations!
+
Click the `Run` button to launch the Python 3 installer.
+
image:i/win-install-0-security-warning.png[WindowsSecurity,409,309]

. The first question the installer will ask you is whether you want to install Python 3 for all users or just for you. The default choice is “install for all users,” which is the best choice unless you have a good reason to choose otherwise. (One possible reason why you would want to “install just for me” is that you are installing Python on your company’s computer and you don’t have administrative rights on your Windows account. But then, why are you installing Python without permission from your company’s Windows administrator? Don’t get me in trouble here!)
+
Click the `Next` button to accept your choice of installation type.
+
image:i/win-install-1-all-users-or-just-me.png["Python installer: select whether to install Python 3.1 for all users of this computer",width=499,height=432]


. Next, the installer will prompt you to choose a destination directory.
The default for all versions of Python 3.1.x is `C:\Python31\`, which
should work well for most users unless you have a specific reason to
change it. If you maintain a separate drive letter for installing
applications, you can browse to it using the embedded controls, or
simply type the pathname in the box below. You are not limited to
installing Python on the `C:` drive; you can install it on any drive, in
any folder.
+
Click the `Next` button to accept your choice of destination directory.
+
image:i/win-install-2-destination-directory.png["Python installer: select destination directory",width=499,height=432]


. The next page looks complicated, but it’s not really. Like many installers, you have the option not to install every single component of Python 3. If disk space is especially tight, you can exclude certain
components.
+
image:i/win-install-3-customize.png["Python installer: customize Python 3.1",width=499,height=432]

	* *Register Extensions* allows you to double-click Python scripts (`.py`
files) and run them. Recommended but not required. (This option doesn’t
require any disk space, so there is little point in excluding it.)
	* *Tcl/Tk* is the graphics library used by the Python Shell, which you
will use throughout this book. I strongly recommend keeping this option.
	* *Documentation* installs a help file that contains much of the
information on http://docs.python.org/[`docs.python.org`]. Recommended
if you are on dialup or have limited Internet access.
	* *Utility Scripts* includes the `2to3.py` script which you’ll learn
about link:case-study-porting-chardet-to-python-3.html[later in this
book]. Required if you want to learn about migrating existing Python 2
code to Python 3. If you have no existing Python 2 code, you can skip
this option.
	* *Test Suite* is a collection of scripts used to test the Python
interpreter itself. We will not use it in this book, nor have I ever
used it in the course of programming in Python. Completely optional.


. If you’re unsure how much disk space you have, click the `Disk Usage`
button. The installer will list your drive letters, compute how much
space is available on each drive, and calculate how much would be left
after installation.
+
Click the `OK` button to return to the “Customizing Python” page.
+
image:i/win-install-3a-disk-usage.png["Python installer: disk space requirements",width=499,height=432]

. If you decide to exclude an option, select the drop-down button before
the option and select “Entire feature will be unavailable.” For example,
excluding the test suite will save you a whopping 7908KB of disk space.
+
Click the `Next` button to accept your choice of options.
+
image:i/win-install-3b-test-suite.png["Python installer: removing Test Suite option will save 7908KB on your hard drive",width=499,height=432]

. The installer will copy all the necessary files to your chosen destination directory. (This happens so quickly, I had to try it three times to even get a screenshot of it!)
+
image:i/win-install-4-copying.png["Python installer: progress meter",width=499,height=432]

. Click the `Finish` button to exit the installer.
+
image:i/win-install-5-finish.png["Python installer: installation completed. Special Windows thanks to Mark Hammond, without whose years of freely shared Windows expertise, Python for Windows would still be Python for DOS.",width=499,height=432]

. In your `Start` menu, there should be a new item called `Python 3.1`.
Within that, there is a program called IDLE. Select this item to run the
interactive Python Shell.
+
image:i/win-interactive-shell.png["Windows Python Shell, a graphical interactive shell for Python",width=677,height=715]

Skip to <<idle, using the Python Shell>>.
