[[idle]]
=== Using The Python Shell

The Python Shell is where you can explore Python syntax, get interactive
help on commands, and debug short programs. The graphical Python Shell
(named IDLE) also contains a decent text editor that supports Python
syntax coloring and integrates with the Python Shell. If you don’t
already have a favorite text editor, you should give IDLE a try.

First things first. The Python Shell itself is an amazing interactive
playground. Throughout this book, you’ll see examples like this:

[source,console]
----
>>> 1 + 1
2
----

The three angle brackets, >>>, denote the Python Shell prompt. Don’t
type that part. That’s just to let you know that this example is meant
to be followed in the Python Shell.

1 + 1 is the part you type. You can type any valid Python expression or
command in the Python Shell. Don’t be shy; it won’t bite! The worst that
will happen is you’ll get an error message. Commands get executed
immediately (once you press ENTER); expressions get evaluated
immediately, and the Python Shell prints out the result.

2 is the result of evaluating this expression. As it happens, 1 + 1 is a
valid Python expression. The result, of course, is 2.

Let’s try another one.

[source,console]
----
>>> print('Hello world!')
Hello world!
----

Pretty simple, no? But there’s lots more you can do in the Python shell.
If you ever get stuck — you can’t remember a command, or you can’t
remember the proper arguments to pass a certain function — you can get
interactive help in the Python Shell. Just type help and press ENTER.

[source,console]
----
>>> help
Type help() for interactive help, or help(object) for help about object.
----

There are two modes of help. You can get help about a single object,
which just prints out the documentation and returns you to the Python
Shell prompt. You can also enter __help mode__, where instead of
evaluating Python expressions, you just type keywords or command names
and it will print out whatever it knows about that command.

To enter the interactive help mode, type help() and press ENTER.

[source,console]
----
>>> help()
Welcome to Python 3.0!  This is the online help utility.

If this is your first time using Python, you should definitely check out
the tutorial on the Internet at http://docs.python.org/tutorial/.

Enter the name of any module, keyword, or topic to get help on writing
Python programs and using Python modules.  To quit this help utility and
return to the interpreter, just type "quit".

To get a list of available modules, keywords, or topics, type "modules",
"keywords", or "topics".  Each module also comes with a one-line summary
of what it does; to list the modules whose summaries contain a given word
such as "spam", type "modules spam".

help>
----

Note how the prompt changes from >>> to help>. This reminds you that
you’re in the interactive help mode. Now you can enter any keyword,
command, module name, function name — pretty much anything Python
understands — and read documentation on it.

[source,console]
----
help> print # <1>
Help on built-in function print in module builtins:

print(...)
    print(value, ..., sep=' ', end='\n', file=sys.stdout)

    Prints the values to a stream, or to sys.stdout by default.
    Optional keyword arguments:
    file: a file-like object (stream); defaults to the current sys.stdout.
    sep:  string inserted between values, default a space.
    end:  string appended after the last value, default a newline.

help> PapayaWhip # <2>
no Python documentation found for 'PapayaWhip'

help> quit # <3>

You are now leaving help and returning to the Python interpreter.
If you want to ask for help on a particular object directly from the
interpreter, you can type "help(object)".  Executing "help('string')"
has the same effect as typing a particular string at the help> prompt.
>>> # <4>
----

<1> To get documentation on the `print()` function, just type print and
press ENTER. The interactive help mode will display something akin to a
man page: the function name, a brief synopsis, the function’s arguments
and their default values, and so on. If the documentation seems opaque
to you, don’t panic. You’ll learn more about all these concepts in the
next few chapters.
<2> Of course, the interactive help mode doesn’t know everything. If you
type something that isn’t a Python command, module, function, or other
built-in keyword, the interactive help mode will just shrug its virtual
shoulders.
<3> To quit the interactive help mode, type quit and press ENTER.
<4> The prompt changes back to >>> to signal that you’ve left the
interactive help mode and returned to the Python Shell.

IDLE, the graphical Python Shell, also includes a Python-aware text
editor.
